<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
    </head>
    <body>
<?php 
 
session_start();
if($_SESSION["isloggedin"]){
?>
    
    <h1> Update book </h1>
    <form method="post" action="book_updated.php">
    <label for="id"> Book id of book to be updated: </label>
    <input type="number" name="id" required/>
    <label for="name"> New book name: </label>
    <input type="text" name="name" required/>
    <label for="author"> New author name: </label>
    <input type="text" name="author"/>
    <label for="date"> New publication date: </label>
    <input type="date" name="date"/>
    <input type="submit">
    </form>
<?php     
} else {
?>
    <div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


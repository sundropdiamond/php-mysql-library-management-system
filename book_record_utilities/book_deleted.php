<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />

<?php 

include "../database_utilities.php";
include "../constants.php";

$id = $_POST["id"];

$conn = ConnectDatabase();

$sql = "delete from $books_table_name where id=$id";
$result = mysqli_query($conn, $sql);
if($result){
    echo "<div class='result'> Book successfully deleted! </div>";
} else {
    echo "<div class='result'> Error in deletion! </div>";
}

CloseConnection($conn);
?>
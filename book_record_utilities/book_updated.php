<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />

<?php 

include "../database_utilities.php";
include "../constants.php";

$id = $_POST["id"];
$name = $_POST["name"];
$author = $_POST["author"];
$date = $_POST["date"];

$conn = ConnectDatabase();

$sql = "update $books_table_name set name='$name', "." author=".($author? "'$author', ": "NULL, ")." publication_date=".($date? "'$date'": "NULL")." where id=$id";
$result = mysqli_query($conn, $sql);
if($result){
    echo "<div class='result'> Book successfully updated! </div>";
} else {
    echo "<div class='result'> Error in updation! </div>";
}

CloseConnection($conn);
?>
<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
    </head>
    <body>
<?php 

session_start();
if($_SESSION["isloggedin"]){
?>
    
    <h1> Add book </h1>

    <form method="post" action="book_added.php">
    <label for="name"> Book name: </label>
    <input type="text" name="name" required/>
    <label for="author"> Author name: </label>
    <input type="text" name="author"/>
    <label for="date"> Publication date: </label>
    <input type="date" name="date"/>
    <input type="submit">
    </form>
<?php     
} else {
?>
    <div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />

<?php 

include "../database_utilities.php";
include "../constants.php";

$name = $_POST["name"];
$author = $_POST["author"];
$date = $_POST["date"];

$conn = ConnectDatabase();

$sql = "insert into $books_table_name(name, author, publication_date) values('$name', ".($author? "'$author', ": "NULL, ").($date? "'$date'": "NULL").")";
$result = mysqli_query($conn, $sql);
if($result){
    echo "<div class='result'> New book successfully added! </div>";
} else {
    echo "<div class='result'> Error in insertion! </div>";
}

CloseConnection($conn);
?>
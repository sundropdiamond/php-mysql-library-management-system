<html>
    <head>
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
    </head>
    <body>
<?php 
 
session_start();
if($_SESSION["isloggedin"]){
?>    
    <h1> Delete book </h1>
    <form method="post" action="book_deleted.php">
    <label for="id"> Book id: </label>
    <input type="text" name="id" required/>
    </form>
<?php     
} else {
?>
    <div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


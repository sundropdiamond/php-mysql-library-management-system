<html>
<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
</head>
<body>

<?php 
include "../database_utilities.php";
include "../constants.php";

session_start();
if($_SESSION["isuserloggedin"] || $_SESSION["isloggedin"]){       
    $conn = ConnectDatabase();
    $sql = "select * from $books_table_name";
    $result = mysqli_query($conn, $sql);
    if($result){
        echo "<h1> View all book records </h1>";
    } else {
        echo "<div class='result'> Error in updation! </div>";
    }
    echo "<table> <th> Book id </th> <th> Book name </th> <th> Author name </th> <th> Publication date </th>";
    while($row = mysqli_fetch_assoc($result)){
        $id = $row['id'];
        $name = $row['name'];
        $author = $row['author'];
        $date = $row['publication_date'];
        echo "<tr> <td> $id </td> <td> $name </td> <td> $author </td> <td> $date </td> </tr>";
    }
    echo "</table>";
    CloseConnection($conn);
} else {
?>
    <div class="result"> You are not logged in </div>
    <button> <a href="../user_login.php"> Login as user </a> </button>
    <button> <a href="../admin_login.php"> Login as admin </a> </button>
<?php
}
?>

</body>
</html>


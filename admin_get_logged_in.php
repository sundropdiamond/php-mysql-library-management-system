<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />

<?php 
include "constants.php";
$username = $_POST["username"];
$password = $_POST["password"];

if($username){
    if($username == $admin_username && $password == $admin_password){
        session_start();
        $_SESSION["isloggedin"] = "yes";
?>
        <button> <a href="admin_index.php"> Administrator panel </a> </button>
<?php
    } else {
?>
        <div class="result"> Please enter your correct username and password. </div>
<?php
    }
} else {
?>
        <div class="result"> You have not entered the required details. </div>
<?php 
}

?>
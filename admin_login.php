<html>
<head>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
</head>
<body>
<?php 

session_start();
if($_SESSION["isloggedin"] || $_SESSION["isuserloggedin"]){
?>
    <div class="result"> You are already logged in. </div>
    <button> <a href="logout.php"> Logout </a> </button>
<?php
} else {
?>
<h1> Administrator Login </h1>
<form method="post" action="admin_get_logged_in.php">
    <label for="username"> Username: </label>
    <input type="text" name="username" required/>
    <label for="password"> Password: </label>
    <input type="password" name="password" required/>
    <input type="submit" value="Login" />
</form>
</body>

</html>
<?php
}

 ?>
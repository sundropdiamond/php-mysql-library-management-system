This is a library management system to be used by a librarian/library administrator or a library member that uses PHP/MySQL for backend and HTML/CSS for frontend. The librarian/administrator can add, delete and modify book records and delete user records. Ze can also store records of which books have been issued to which user. A user can register to create a unique account and login whenever they wish to borrow or return a book. A user at a time can have upto two books issued.

To run the program correctly, go to your terminal and:
1. To restart nginx and php
sudo /etc/init.d/nginx restart
sudo /etc/init.d/php7.2-fpm start
2. Clone this repository in your /var/www/html folder (assuming you use nginx)
3. Change $db_user and $db_password to the user and password corresponding to your MySQL connection.(currently they are "root" and "rootpassword")
4. Run the following in MySQL:
create database library;
use library;
create table books(id int auto_increment, name varchar(100) not null, author varchar(50), publication_date date, primary key(id));
create table members(id int auto_increment, name varchar(100) not null, phone int(10), email varchar(150) not null, password varchar(50) not null, primary key(id));
create table issue_records(id int auto_increment primary key, book_id int, member_id int, issue_date date not null, return_date date, foreign key(book_id) references books(id), foreign key(member_id) references members(id));   
5. 
Go to http://localhost/php-mysql-library-management-system/home_page.php to go to the home page
6. 
Go to http://localhost/php-mysql-library-management-system/admin_login.php to login as administrator using:
username: admin
password: adminpassword
7. 
Go to http://localhost/php-mysql-library-management-system/user_register.php to register as new user
8. 
Go to http://localhost/php-mysql-library-management-system/user_login.php to login as a user
9.
Go to http://localhost/php-mysql-library-management-system/admin_index.php to view the administrator panel
10.
Go to http://localhost/php-mysql-library-management-system/user_index.php to view the user panel

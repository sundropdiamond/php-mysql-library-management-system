<html>

<head> 
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
</head>

<body>

<?php 
session_start();
if($_SESSION["isuserloggedin"]){
?>
    <h1> User panel </h1>

    <ul>
        <li> <a href = "book_record_utilities/view_books.php"> View all book records </a> </li>
        <li> <a href = "transactions/issue_book.php"> Borrow book </a> </li>
        <li> <a href = "transactions/return_book.php"> Return book </a> </li>
    </ul>
    <button> <a href="logout.php"> Logout </a> </button>
<?php
} else { 
?>
    <div class="result"> You are not logged in as a user </div>
    <button> <a href="user_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


<html>
<head>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
</head>
<body>
<?php 

session_start();
if($_SESSION["isloggedin"] || $_SESSION["isuserloggedin"]){
?>
    <div class="result"> You are already logged in. </div>
    <button> <a href="logout.php"> Logout </a> </button>
<?php
} else {
?>
<h1> Register as a new user </h1>

<form action="user_get_registered.php" method="post">
    <label for="name"> Member name: </label>
    <input type="text" name="name" required/>
    <label for="email"> Email: </label>
    <input type="email" name="email" required/>
    <label for="phone"> Phone: </label>
    <input type="number" name="phone"/>
    <label for="password"> Password: </label>
    <input type="password" name="password" required/>
    <label for="password"> Repeat password: </label>
    <input type="password" name="repeated_password" required/>
    <input type="submit"/>
</form>

</body>

</html>
<?php
}

 ?>

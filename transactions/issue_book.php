<html>
<head>
     <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
</head>
<body>

<?php 

session_start();
if($_SESSION["isuserloggedin"]){
?>
    
    <h1> Issue book </h1>
    <form action="book_issued.php"  method="post">
    <label for="book_id"> Book id </label>
    <input type="number" name="book_id" required/>
    <input type="submit" />
    </form>
<?php     
} else {
?>
    <div class="result"> You are not logged in as a user </div>
    <button> <a href="../user_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />

<?php 
include "../database_utilities.php";
include "../constants.php";

session_start();
$member_id = $_SESSION["id"];
$book_id = $_POST["book_id"];

if($book_id && $member_id){
    $conn = ConnectDatabase();

    $query = "select * from $issue_table_name where member_id=$member_id and book_id=$book_id and return_date is NULL";
    $result = mysqli_query($conn, $query);

    if(mysqli_num_rows($result)){
        $date = date('Y-m-d');
        $sql = "update $issue_table_name set return_date='$date' where member_id=$member_id and book_id=$book_id and return_date is NULL";
        $sql_result = mysqli_query($conn, $sql);
        echo "<div class='result'> The required book has been returned. </div>";
    } else {
        echo "<div class='result'> No book with given id has been borrowed by member. </div>";
    }

    CloseConnection($conn);
}

?>
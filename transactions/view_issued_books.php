<html>
<head>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
</head>
<body>

<?php 
 
session_start();
if($_SESSION["isloggedin"]){
?>

<h1> View all issue records </h1>

<?php 

include "../database_utilities.php";
include "../constants.php";
$conn = ConnectDatabase();

$query = "select * from $issue_table_name";
$result = mysqli_query($conn, $query);

echo "<table> <tr>";
echo "<th> SNo </th> <th> Book id </th> <th> Member id </th> <th> Issue date </th> <th> Return date </th>";
echo "</tr>";

while($row = mysqli_fetch_assoc($result)){
    $id = $row["id"];
    $b_id = $row["book_id"];
    $m_id = $row["member_id"];
    $date = $row["issue_date"];
    $date2 = $row["return_date"];

    echo "<tr> <td> $id </td> <td> $b_id </td> <td> $m_id </td> <td> $date </td> <td> $date2 </td> </tr>";
}

echo "</table>";

CloseConnection($conn);
} else {
?>

<div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login as admin </a> </button>
<?php
}
?>


</body>
</html>


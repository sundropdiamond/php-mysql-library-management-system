<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />

<?php 
include "../database_utilities.php";
include "../constants.php";

session_start();
$member_id = $_SESSION["id"];
$book_id = $_POST["book_id"];

if($book_id && $member_id){
    $conn = ConnectDatabase();

    $query = "select * from $issue_table_name where book_id=$book_id and return_date is NULL";
    $result = mysqli_query($conn, $query);

    if(mysqli_num_rows($result)){
        echo "<div class='result'> The requested book is unavailable. </div>";
    } else {
        $has_borrowed_query = "select * from $issue_table_name where member_id=$member_id and return_date is NULL";
        $has_borrowed_result = mysqli_query($conn, $has_borrowed_query);
        if(mysqli_num_rows($has_borrowed_result) == 2){
            echo "<div class='result'> Member cannot borrow more than two books at a time! </div>";
        } else {
            $date = date('Y-m-d');
            $borrow_query = "insert into $issue_table_name(book_id, member_id, issue_date, return_date) values($book_id, $member_id, '$date', NULL)";
            $borrow_result = mysqli_query($conn, $borrow_query);
            if($borrow_result){
                echo "<div class='result'> Book successfully borrowed! </div>";
            } else {
                echo "<div class='result'> Error in insertion! </div>";
            }
        }
    }

    CloseConnection($conn);
}

?>
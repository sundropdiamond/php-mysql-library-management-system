<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
<?php 

session_start();
session_destroy();
?>
<div class="result"> You are now logged out. </div>
<button> <a href="home_page.php"> Home </a> </button>
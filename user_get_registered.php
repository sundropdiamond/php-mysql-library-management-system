<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />

<?php 
include "database_utilities.php";
include "constants.php";

$name = $_POST["name"];
$phone = $_POST["phone"];
$email = $_POST["email"];
$password = $_POST["password"];
$repeated_password = $_POST["repeated_password"];

$conn = ConnectDatabase();
$query = "select * from $members_table_name where email='$email'";
$result = mysqli_query($conn, $query);

if($name){
    $num_rows = mysqli_num_rows($result);
    if($num_rows == 0 && $password == $repeated_password){
        session_start();
        $_SESSION["isuserloggedin"] = "yes";
        
        $store_query;
        if($phone){
            $store_query = "insert into $members_table_name(name, email, password, phone) values('$name', '$email', md5('$password'), $phone)";
        } else {
            $store_query = "insert into $members_table_name(name, email, password) values('$name', '$email', md5('$password'))";
        }

        $store_result = mysqli_query($conn, $store_query);
        if($store_result){
            echo "<div class='result'>Registered successfully!</div>";
            $select_query = "select * from $members_table_name where email='$email'";
            $select_result = mysqli_query($conn, $select_query);
            $row = mysqli_fetch_assoc($select_result);
            $_SESSION["id"] = $row["id"];
?>
            <button> <a href="user_index.php"> User panel </a> </button> 
<?php    
        } else {
            echo "<div class='result'>Error in registration!</div>";
        }
    } else if ($num_rows == 0){
?>
        <div class="result"> Please enter the password correctly. </div>
<?php
    } else {
?>
        <div class="result"> That email is already taken. Please enter your email id correctly. </div>
<?php
    }
} else {
?>
        <div class="result"> You have not entered the required details. </div>
<?php 
}

CloseConnection($conn);
?>
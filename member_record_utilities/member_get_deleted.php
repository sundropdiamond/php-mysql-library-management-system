<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
<?php 

$id = $_POST["id"];

include "../database_utilities.php";
include "../constants.php";
$conn = ConnectDatabase();

$query = "delete from $members_table_name where id=$id";
$result = mysqli_query($conn, $query);

echo "<div class='result'> The required member has been deleted. </div>";

CloseConnection($conn);

?>
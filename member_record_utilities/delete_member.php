<html>
<head>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
</head>
<body>

<?php 
 
session_start();
if($_SESSION["isloggedin"]){
?>
    
    <h1> Delete member </h1>
    <form action="member_get_deleted.php" method="post">
    <label for="id"> Member id </label>
    <input type="number" name="id" required/>
    <input type="submit"/>
    </form>
<?php     
} else {
?>
    <div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


<html>
<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../style.css" />
</head>
<body>

<?php 
 
session_start();
if($_SESSION["isloggedin"]){
?>

<h1> View all members </h1>

<?php 

include "../database_utilities.php";
include "../constants.php";
$conn = ConnectDatabase();

$query = "select * from $members_table_name";
$result = mysqli_query($conn, $query);

echo "<table> <th> Member id </th> <th> Member name </th> <th> Email id </th> <th> Phone no </th>";

while($row = mysqli_fetch_assoc($result)){
    $id = $row["id"];
    $name = $row["name"];
    $email = $row["email"];
    $phone = $row["phone"];

    echo "<tr> <td> $id </td> <td> $name </td> <td> $email </td> <td> $phone </td> </tr>";
}

echo "</table>";

CloseConnection($conn);
} else {
?>

<div class="result"> You are not logged in as admin </div>
    <button> <a href="../admin_login.php"> Login as admin </a> </button>
<?php
}
?>


</body>
</html>


<html>
<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
</head>
<body>

<?php 
session_start();
if($_SESSION["isloggedin"]){
?>
    <h1> Administrator panel </h1>

    <ul>
        <li> <a href = "book_record_utilities/view_books.php"> View all book records </a> </li>
        <li> <a href = "book_record_utilities/add_book.php"> Add book record </a> </li>
        <li> <a href = "book_record_utilities/delete_book.php"> Delete book record </a> </li>
        <li> <a href = "book_record_utilities/update_book.php"> Modify book record </a> </li>
        <li> <a href = "member_record_utilities/view_members.php"> View all member records </a> </li>
        <li> <a href = "member_record_utilities/delete_member.php"> Delete member record </a> </li>
        <li> <a href = "transactions/view_issued_books.php"> View all records of issued books </a> </li>
    </ul>
    <button> <a href="logout.php"> Logout </a> </button>
<?php
} else { 
?>
    <div class="result"> You are not logged in as admin </div>
    <button> <a href="admin_login.php"> Login </a> </button>
<?php
}
?>

</body>
</html>


<?php 

include "constants.php";

function ConnectDatabase(){
    global $db_user;
    global $db_password;
    global $db_name;
    $conn = mysqli_connect("localhost", $db_user, $db_password, $db_name);
    return $conn;
}

function CloseConnection($conn){
    $conn -> close();
}

?>
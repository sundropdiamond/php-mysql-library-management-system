<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="style.css" />
<?php 

session_start();
if($_SESSION["isloggedin"] || $_SESSION["isuserloggedin"]){
?>
    <div class="result"> You are already logged in. </div>
    <button> <a href="logout.php"> Logout </a> </button>
<?php
} else {
?>
<html>

<body>
<h1> Login </h1>

<form action="user_get_logged_in.php" method="post">
    <label for="email"> Email: </label>
    <input type="email" name="email" required/>
    <label for="password"> Password: </label>
    <input type="password" name="password" required/>
    <input type="submit"/>
</form>

</body>

</html>
<?php
}

 ?>